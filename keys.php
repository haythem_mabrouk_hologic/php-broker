<?php
require 'vendor/autoload.php';

use Hmabrouk\PhpLock\Broker\Redis as RedisAlias;

$redis = RedisAlias::get();

$list = $redis->keys("*");
if (0 == count($list)) {
    echo "No Keys \n";
}
foreach ($list as $key) {
    $range = $redis->lRange($key, 0, -1);
    $count = is_array($range)? count($range): false;
    printf("%s has %d values \n", $key, $count);
}
die;