<?php

use Hmabrouk\PhpLock\Broker\Redis as RedisAlias;

require 'vendor/autoload.php';

// index file is for tests

$redis = RedisAlias::get();
$redis->rPush('key1', 'A');
$redis->rPush('key1', 'B');
$redis->rPush('key1', 'C');
$redis->rPop('key1');
$redis->rPop('key1');


print_r($redis->keys("*"));