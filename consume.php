<?php

use Hmabrouk\PhpLock\Broker\Consumer;
use Hmabrouk\PhpLock\BusinessLogic\Action;
use Hmabrouk\PhpLock\BusinessLogic\Logger;
use Hmabrouk\PhpLock\Entity\PlatformService;

include 'vendor/autoload.php';
$name = $argv[1] ?? 'un';
PlatformService::consoleMode();
$consumer = new Consumer([new Action($name)]);
$consumer->consume();