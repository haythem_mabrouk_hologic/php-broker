<?php

use Hmabrouk\PhpLock\Broker\BrokerConsumer;
use Hmabrouk\PhpLock\Broker\Producer;
use Hmabrouk\PhpLock\Entity\JobRepository;
use Hmabrouk\PhpLock\Conf;

include 'vendor/autoload.php';
use Hmabrouk\PhpLock\Entity\Job;
use Hmabrouk\PhpLock\Entity\PlatformService;

PlatformService::consoleMode();
$plateformRepo = new \Hmabrouk\PhpLock\Entity\PlatformRepository();
$platforms = PlatformService::getListPlatforms();
$producers = [];
//foreach ($platforms as $platform) {
//    $producer
//}

$jobRepo = new JobRepository();
$number = $argv[1] ?? 10;
for ($i = 0; $i < $number; $i++) {
    $jobRepo->add(new Job());
    $plateform = PlatformService::getObject()->getPlatformRepository()->getByIndex(rand(0, count($platforms)-1));
    $brokerName = BrokerConsumer::BROKER_NAME_PREFIX_JOBS.$plateform->getName();
    $producer = new Producer($brokerName);
    $producer->produce($jobRepo->getByIndex($i)->getId());
}
