<?php

namespace Hmabrouk\PhpLock\Entity;

class Job extends AbstractEntity
{
    const STATUS_WAITING = 'Waiting';
    const STATUS_IN_PROGRESS = 'In progress ..';
    const STATUS_SUCCESS = 'Success';
    const STATUS_Error = 'Error';

    private ?string $status;

    public function __construct()
    {
        $this->status = self::STATUS_WAITING;
    }

    public function updateStatus(string $status): void
    {
        if (in_array($status, [self::STATUS_WAITING, self::STATUS_IN_PROGRESS, self::STATUS_SUCCESS, self::STATUS_Error])) {
            $this->status = $status;
        }
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

}