<?php

namespace Hmabrouk\PhpLock\Entity;

class PlatformService
{
    const SELECTED_PLATFORM_MODE = 'selected';
    const CONSOLE_PLATFORM_MODE = 'console';

    private static self $obj;

    private PlatformRepository $platformRepository;
    private ?string $context = null;

    private function __construct(?PlatformRepository $platformRepository = null, ?string $context = null)
    {
        $this->platformRepository = $platformRepository ?? new PlatformRepository();
        $this->context = $context;
    }


    public static function getObject()
    {
        if (!isset(self::$obj)) {
            self::$obj = new PlatformService();
        }
        return self::$obj;
    }

    public static function consoleMode(): void
    {

        $platformRepository = new PlatformRepository();
        $platformRepository
            ->add(new Platform('endive'))
            ->add(new Platform('fenouil'));

        self::$obj = new PlatformService($platformRepository, self::CONSOLE_PLATFORM_MODE);
    }

    public static function webMode()
    {
        $platformRepository = new PlatformRepository();
        $platformRepository
            ->add(new Platform('fenouil'));
        self::$obj = new PlatformService($platformRepository, self::SELECTED_PLATFORM_MODE);
    }

    /**
     * @return Platform[]
    */
    public static function getListPlatforms(): array
    {
        return self::getObject()->platformRepository->getItems();
    }

    /**
     * @return PlatformRepository
     */
    public function getPlatformRepository(): PlatformRepository
    {
        return $this->platformRepository;
    }



    /**
     * @return string
     */
    public function getContext(): string
    {
        return $this->context;
    }

}