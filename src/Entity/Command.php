<?php

namespace Hmabrouk\PhpLock\Entity;

class Command
{
    private int $id;
    private CommandType $commandType;
    private CommandAttributes $commandAttributes;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return CommandType
     */
    public function getCommandType(): CommandType
    {
        return $this->commandType;
    }

    /**
     * @param CommandType $commandType
     */
    public function setCommandType(CommandType $commandType): void
    {
        $this->commandType = $commandType;
    }

    /**
     * @return CommandAttributes
     */
    public function getCommandAttributes(): CommandAttributes
    {
        return $this->commandAttributes;
    }

    /**
     * @param CommandAttributes $commandAttributes
     */
    public function setCommandAttributes(CommandAttributes $commandAttributes): void
    {
        $this->commandAttributes = $commandAttributes;
    }


}