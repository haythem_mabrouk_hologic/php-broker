<?php

namespace Hmabrouk\PhpLock\Entity;

class JobRepository extends AbstractRepository
{

    public function getByStatus(string $status): array
    {
        $list = [];
        foreach ($this->items as $item) {
            if($status == $item->getStatus) {
                $list[] = $item;
            }
        }

        return $list;
    }

}