<?php

namespace Hmabrouk\PhpLock\Entity;

class Platform extends AbstractEntity
{

    public function __construct(private string $name, protected ?int $id = null){}

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

}