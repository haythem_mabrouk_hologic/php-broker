<?php

namespace Hmabrouk\PhpLock\Entity;

use JetBrains\PhpStorm\Pure;

class PlatformRepository extends AbstractRepository
{
    /**
     * @param string $name
     * @return Platform|null
     */
    public function getByName(string $name): Platform|null
    {
        /** @var Platform $item*/
        foreach ($this->items as $item)
        {
            if($name === $item->getName()) {
                return $item;
            }
        }
        return null;
    }


}