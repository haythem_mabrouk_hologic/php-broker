<?php

namespace Hmabrouk\PhpLock\Entity;

abstract class AbstractRepository
{
    /* @var array<AbstractEntity>*/
    protected array $items = [];

    public function add(AbstractEntity $item): self
    {
        $item->setId(count($this->items));
        $this->items[] = $item;

        return $this;
    }

    public function getByIndex(int $index): ?AbstractEntity
    {
        if(isset($this->items[$index])) {
            return $this->items[$index];
        }
        return null;
    }

    /**
     * @return AbstractEntity[]
     */
    public function getItems(): array
    {
        return $this->items;
    }
}