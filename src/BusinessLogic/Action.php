<?php

namespace Hmabrouk\PhpLock\BusinessLogic;

class Action
{
    const PATTERN_PLATFORM_FILENAME = 'platform';
    const FILENAME = self::PATTERN_PLATFORM_FILENAME.'_toto.txt';
    private string $idJob;
    private string $filename;

    public function __construct(private string $name)
    {
    }


    public function action(?string $filePrefix = null)
    {
        $this->filename = str_replace(self::PATTERN_PLATFORM_FILENAME, $filePrefix, self::FILENAME);
        $this->session_ssh();
        sleep(1);
        $this->exec_script();
        sleep(5);
    }

    public function log_it()
    {
        file_put_contents($this->filename, "log \n", FILE_APPEND);
        sleep(1);
    }

    /**
     * @param string $idJob
     */
    public function setIdJob(string $idJob): void
    {
        $this->idJob = $idJob;
    }

    private function session_ssh()
    {
        file_put_contents($this->filename, sprintf("%s : %s  - ", $this->name, $this->idJob), FILE_APPEND);
    }

    private function exec_script()
    {
        file_put_contents($this->filename, sprintf("%s : %s\n", $this->idJob, $this->name), FILE_APPEND);
    }
}