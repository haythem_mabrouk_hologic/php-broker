<?php

namespace Hmabrouk\PhpLock\BusinessLogic;

interface LoggerInterface
{

    public function error($message, array $context = array());
}
