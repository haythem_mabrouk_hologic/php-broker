<?php

namespace Hmabrouk\PhpLock\BusinessLogic\Caller;

interface CallerInterface
{
    public function canBeCalled(...$arguments): bool;
    public function call(...$arguments): bool;
}