<?php

namespace Hmabrouk\PhpLock\BusinessLogic\Caller;

use Hmabrouk\PhpLock\Entity\CommandType;

class SshCaller implements CallerInterface
{

    public function canBeCalled(...$arguments): bool
    {
        return $arguments[0] instanceof CommandType && CommandType::NAME_SSH == $arguments[0]->getName());
    }

    public function call(...$arguments): bool
    {
        // TODO: Implement call() method.
        // logic ssh call
    }
}