<?php

namespace Hmabrouk\PhpLock\Broker;

class BrokerConsumer extends Broker
{
    const BROKER_NAME_PREFIX_PLATFORM = 'platform_';
    const BROKER_NAME_PREFIX_JOBS = 'jobs_';


    private BrokerLock $brokerLock;

    public function __construct(string $suffixOrName, string $platformName)
    {
        parent::__construct($suffixOrName.$platformName);
        $this->brokerLock = new BrokerLock($this->getQueueName());
    }

    public function isLocked(): bool
    {
        return $this->brokerLock->isLocked();
    }

    public function lock(?string $value = null)
    {
        $this->brokerLock->lock($value);
    }

    public function getValue(): mixed
    {
        $range = $this->redis->lRange($this->queueName, -1, -1);
        if(is_array($range) && !empty($range)) {
            return $range[0];
        }
        return null;
    }

    public function isEmpty(): bool
    {
        return  0 == $this->countValues();
    }

    public function unlock()
    {
        $this->brokerLock->unlock();
    }

    public function remValue(mixed $value)
    {
        $this->redis->lRem($this->queueName, $value, 1);
    }


}