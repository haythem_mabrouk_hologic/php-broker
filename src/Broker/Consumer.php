<?php

namespace Hmabrouk\PhpLock\Broker;

use Exception;
use Hmabrouk\PhpLock\Broker\Exception\LockException;
use Hmabrouk\PhpLock\BusinessLogic\Logger;
use Hmabrouk\PhpLock\Entity\PlatformService;
use Hmabrouk\PhpLock\BusinessLogic\LoggerInterface;

class Consumer
{
    public function __construct(private array $actions = [], private LoggerInterface $logger = new Logger())
    {
    }

    public function consume()
    {

        $platforms = PlatformService::getListPlatforms();
        foreach ($platforms as $platform) {
            $platformConsumer = new BrokerConsumer(BrokerConsumer::BROKER_NAME_PREFIX_PLATFORM, $platform->getName());
            $jobConsumer = new BrokerConsumer(BrokerConsumer::BROKER_NAME_PREFIX_JOBS, $platform->getName());
            if ($platformConsumer->isLocked()) {
                continue;
            }
            try {
                $platformConsumer->lock();
            } catch (LockException) {
                print "29 LockException \n";
                continue;
            }
            while ($jobConsumer->countValues()) {
                $jobId = $jobConsumer->getValue();

                if (!$jobConsumer->isLocked())
                    try {
                        $jobConsumer->lock($jobId);
                    } catch (LockException) {
                        print "39 LockException \n";
                        continue;
                    }
                foreach ($this->actions as $action) {
                    try {
                        $action->setIdJob($jobId);
                        //                        var_dump($action);
                        $action->action($platform->getName());

                    } catch (Exception $exception) {
                        $this->logger->error($exception->getMessage());
                    }
                }
                $jobConsumer->unlock();
                $jobConsumer->remValue($jobId);
            }
            sleep(1);
            $platformConsumer->unlock();
        }

    }
}