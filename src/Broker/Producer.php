<?php

namespace Hmabrouk\PhpLock\Broker;


class Producer extends Broker
{

    public function produce( $jobid)
    {
        $pushed = $this->push($jobid);
        printf("%s just pushed  into %s we have now %d values  \n", $jobid, $this->queueName, $this->countQueue());
//        sleep(1);
    }

    protected function push($job)
    {
        return Redis::get()->rPush($this->queueName, $job);
    }
}