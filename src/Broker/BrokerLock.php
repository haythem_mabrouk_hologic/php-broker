<?php

namespace Hmabrouk\PhpLock\Broker;

use Hmabrouk\PhpLock\Broker\Exception\BrokerException;
use Hmabrouk\PhpLock\Broker\Exception\LockException;

class BrokerLock extends Broker
{
    const BROKER_NAME_SUFFIX_LOCK = '_lock';
    const BROKER_NAME_PREFIX_LOCK = 'is_locked_';
    const LOCK_DEFAULT_VALUE = 'locked';

    public function __construct(string $brokerName)
    {
        parent::__construct($brokerName.self::BROKER_NAME_SUFFIX_LOCK);
    }

    public function isLocked(): bool
    {
        return $this->isBrokerNameHasChanged();
    }

    /**
     * @throws LockException
     */
    public function lock(?string $value = self::LOCK_DEFAULT_VALUE)
    {
        if(!$this->redis->lPush($this->queueName, $value)) {
            throw new BrokerException("Unable to lock ".$this->queueName);
        }
        if(!$this->renameBrokerName(self::BROKER_NAME_PREFIX_LOCK.$this->queueName)) {
            throw new LockException();
        }
        printf("locking %s with value %s, total is %d \n", $this->queueName, $value, $this->countValues());

    }

    public function unlock()
    {
        $this->redis->rPop($this->queueName);
        $this->redis->del($this->queueName);
        $this->redis->del(str_replace(self::BROKER_NAME_PREFIX_LOCK, '', $this->queueName));
    }

    private function isBrokerNameHasChanged(): bool
    {
        return str_contains($this->queueName, self::BROKER_NAME_PREFIX_LOCK);
    }

    private function renameBrokerName($brokerName, $forceRemove = false): bool
    {
        if($forceRemove && $this->redis->rename($this->queueName, $brokerName)) {
            $this->queueName = $brokerName;
            ;return  true;
        }
        if(!$forceRemove && $this->redis->renameNx($this->queueName, $brokerName)) {
            $this->queueName = $brokerName;

            return true;
        }
        return false;
    }
}