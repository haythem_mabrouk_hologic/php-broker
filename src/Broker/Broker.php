<?php

namespace Hmabrouk\PhpLock\Broker;

use Hmabrouk\PhpLock\Broker\Redis;
use Redis as NativeRedis;


class Broker
{
    protected ?string $queueName = null;
    protected NativeRedis $redis;

    /**
     * @param string $queueName
     */
    public function __construct(string $queueName)
    {
        $this->queueName = $queueName;
        $this->redis = Redis::get();
    }

    /**
     * @return string|null
     */
    public function getQueueName(): ?string
    {
        return $this->queueName;
    }

    public function countQueue(): int
    {
        return count($this->redis->lRange($this->queueName, 0, -1));
    }

    public function countValues(): int
    {
        $range = $this->redis->lRange($this->queueName, 0, -1);
        return is_array($range) ? count($range): boolval($range);
    }


}