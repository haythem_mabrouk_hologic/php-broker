<?php

namespace Hmabrouk\PhpLock\Broker;

class JobConsumer extends BrokerConsumer
{
    const BROKER_NAME_PREFIX = 'jobs_';

    public function __construct(string $platformName)
    {
        parent::__construct(self::BROKER_NAME_PREFIX.$platformName);
    }
}