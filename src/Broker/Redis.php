<?php

namespace Hmabrouk\PhpLock\Broker;

use Redis as NativeRedis;

class Redis
{
    private static $obj;

    /** @var NativeRedis */
    public $redis;

    private function __construct(){
        $this->redis = new NativeRedis();
    }

    public static function get(): NativeRedis
    {
        if(!self::$obj) {
            self::$obj = new static();
            self::connect();
        }
        return self::$obj->redis;
    }

    private static function connect()
    {
        if(!self::get()->isConnected()) {
            self::get()->connect('127.0.0.1', 6379);
        }
    }
}