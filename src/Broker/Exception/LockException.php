<?php

namespace Hmabrouk\PhpLock\Broker\Exception;

use LogicException;

class LockException extends LogicException
{

}