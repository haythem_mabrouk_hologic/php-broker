<?php

namespace Hmabrouk\PhpLock\Broker\Exception;

use LogicException;

class BrokerException extends LogicException
{

}